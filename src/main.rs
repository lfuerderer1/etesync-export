use etebase::error::Result;
use etebase::managers::CollectionManager;
use etebase::Account;
use etebase::Client;
use etebase::FetchOptions;
use std::fs::File;
use std::io::Write;

const CLIENT_NAME: &str = "etesync_export";

fn prompt(prompt: &str, default: Option<&str>) -> String {
    match default {
        Some(default) => print!("{prompt}[{default}] "),
        None => print!("{prompt}"),
    }
    std::io::stdout()
        .flush()
        .expect("Failed to write console prompt string");
    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line from stdin");
    if let Some(default) = default {
        if input.trim_end() == "" {
            input = default.to_string();
        }
    }
    input.trim().to_string()
}

fn export<F>(
    col_mgr: &CollectionManager,
    collection_type: &str,
    filename: &str,
    header: &[u8],
    separator: &[u8],
    footer: &[u8],
    modifier: F,
) where
    F: Fn(Vec<u8>) -> Vec<u8>,
{
    let collections = col_mgr
        .list(collection_type, None)
        .unwrap_or_else(|_| panic!("Failed to load collections of type {collection_type}"));
    let mut output_file = File::create(filename)
        .unwrap_or_else(|_| panic!("Failed to open output file {filename} for writing"));
    output_file
        .write_all(header)
        .unwrap_or_else(|_| panic!("Error while writing to output file {filename}"));
    let mut first_item = true;
    for col in collections.data() {
        let item_manager = col_mgr.item_manager(col).unwrap_or_else(|_| {
            panic!("Failed to read items in one collection of type {collection_type}")
        });
        // increase the default limit to a high value that (hopefully) will never be reached
        let options = FetchOptions::default().limit(1_000_000);
        let item_list = item_manager.list(Some(&options)).unwrap_or_else(|_| {
            panic!("Failed to read items in one collection of type {collection_type}")
        });
        let items_unordered = item_list.data();
        let mut items_ordered: Vec<_> = items_unordered
            .iter()
            .filter(|item| !item.is_deleted())
            .collect();
        items_ordered.sort_by_key(|item| item.uid());
        for item in items_ordered {
            let content = item.content().unwrap_or_else(|_| {
                panic!("Failed to read content of an item of type {collection_type}")
            });
            if first_item {
                first_item = false;
            } else {
                output_file
                    .write_all(separator)
                    .unwrap_or_else(|_| panic!("Error while writing to output file {filename}"));
            }
            output_file
                .write_all(&modifier(content))
                .unwrap_or_else(|_| panic!("Error while writing to output file {filename}"));
        }
    }
    output_file
        .write_all(footer)
        .unwrap_or_else(|_| panic!("Error while writing to output file {filename}"));
}

fn unpack_calendar_entry(entry: Vec<u8>) -> Vec<u8> {
    let mut entry_slice: &[u8] = &entry;
    entry_slice = match entry_slice.strip_prefix(b"BEGIN:VCALENDAR") {
        Some(slice) => slice,
        None => {
            eprintln!("Warning, invalid format! A calendar entry did not start with \"BEGIN:VCALENDAR\" as expected");
            return Vec::new();
        }
    };
    entry_slice = entry_slice.strip_prefix(b"\r").unwrap_or(entry_slice);
    entry_slice = match entry_slice.strip_prefix(b"\n") {
        Some(slice) => slice,
        None => {
            eprintln!("Warning, invalid format! A calendar entry did not start with \"BEGIN:VCALENDAR\" as expected");
            return Vec::new();
        }
    };

    entry_slice = entry_slice.strip_suffix(b"\n").unwrap_or(entry_slice);
    entry_slice = entry_slice.strip_suffix(b"\r").unwrap_or(entry_slice);
    entry_slice = match entry_slice.strip_suffix(b"\nEND:VCALENDAR") {
        Some(slice) => slice,
        None => {
            eprintln!("Warning, invalid format! A calendar entry did not end with \"END:VCALENDAR\" as expected");
            return Vec::new();
        }
    };
    entry_slice = entry_slice.strip_suffix(b"\r").unwrap_or(entry_slice);

    Vec::from(entry_slice)
}

fn main() -> Result<()> {
    let server_url = prompt(
        "Etebase server url: ",
        Some("https://api.etebase.com/partner/etesync"),
    );
    let username = prompt("Username: ", None);
    let password =
        rpassword::prompt_password("Password: ").expect("Failed to read line from stdin");

    let client = Client::new(CLIENT_NAME, &server_url).expect("Failed to instanciate the client");
    let etebase = Account::login(client, &username, &password).expect("Failed to login");
    let col_mgr = etebase
        .collection_manager()
        .expect("Could not get a collection manager");

    export(
        &col_mgr,
        "etebase.vcard",
        "contacts.vcf",
        b"",
        b"\r\n",
        b"",
        std::convert::identity,
    );
    export(
        &col_mgr,
        "etebase.vevent",
        "calendar.ics",
        b"BEGIN:VCALENDAR\n",
        b"\r\n",
        b"\nEND:VCALENDAR\n",
        unpack_calendar_entry,
    );
    export(
        &col_mgr,
        "etebase.vtodo",
        "tasks.ics",
        b"BEGIN:VCALENDAR\n",
        b"\r\n",
        b"\nEND:VCALENDAR\n",
        unpack_calendar_entry,
    );
    export(
        &col_mgr,
        "etebase.md.note",
        "notes.md",
        b"",
        b"\r\n\r\n----------\r\n\r\n",
        b"",
        std::convert::identity,
    );

    etebase.logout()?;

    Ok(())
}
